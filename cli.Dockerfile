FROM archlinux:base-devel AS builder

RUN pacman -Syu --noconfirm rust

WORKDIR /app
RUN cargo new --lib lib
RUN cargo new cli
COPY Cargo.lock cli/Cargo.toml cli/
COPY lib/Cargo.toml lib/

WORKDIR /app/cli
RUN cargo build --release

COPY lib/src /app/lib/src
RUN touch /app/lib/src/lib.rs
COPY cli/src src
RUN touch src/main.rs
RUN cargo build --release

FROM archlinux:latest
RUN pacman -Syu --noconfirm

COPY --from=builder /app/cli/target/release/aurora-cli /bin/aurora

ENTRYPOINT []

CMD ["/bin/aurora"]
